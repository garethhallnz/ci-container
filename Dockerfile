FROM amazeeio/php:7.2-cli-drupal

RUN apk update \
    && apk upgrade \
    && apk add --no-cache \
        g++ \
        git \
        make \
        ruby \
        ruby-dev \
        ruby-rdoc \
    && rm -rf /var/cache/apk/*

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --filename=composer --install-dir=/usr/bin
ENV COMPOSER_ALLOW_SUPERUSER=1
RUN composer global require hirak/prestissimo
RUN export PATH="$PATH:$HOME/.composer/vendor/bin"

# Install packages
RUN composer global require squizlabs/php_codesniffer
RUN composer global require drupal/coder
RUN ln -s /root/.composer/vendor/bin/phpcs /usr/bin/phpcs
RUN ln -s /root/.composer/vendor/bin/phpcbf /usr/bin/phpcbf
RUN phpcs --config-set installed_paths /home/.composer/vendor/drupal/coder/coder_sniffer
RUN phpcbf --config-set installed_paths /home/.composer/vendor/drupal/coder/coder_sniffer
RUN composer global require phpmd/phpmd    

# Install Cypress
RUN npm install --save-dev cypress

# Install packages
RUN gem install scss_lint

# Get Lint config
RUN git clone https://bitbucket.org/garethhallnz/scss-lint-config.git
RUN cd scss-lint-config/ && git pull
RUN mv scss-lint-config/.scss-lint.yml ~/

# Show versions
RUN php -v
RUN node -v
RUN npm -v
RUN ruby -v
RUN scss-lint -v
RUN /app/node_modules/.bin/cypress -v